const plugin = {
  name: 'solidus-plugin-reviews',
  endpoints: {
    review: {
      endpoints: {
        create: {
          method: 'POST',
          path: '/api/reviews',
        },
        get: {
          method: 'GET',
          path: '/api/reviews/:id',
        },
        update: {
          method: 'PUT',
          path: '/api/reviews/:id',
        },
        delete: {
          method: 'DELETE',
          path: '/api/reviews/:id',
        },
        feedback: {
          endpoints: {
            create: {
              method: 'POST',
              path: '/api/feedback_reviews',
              settings: {
                paramsResolver: {
                  /**
                   * We want to always include the reviewId, even when it's included upstream in
                   * the context.
                   *
                   * @param {object} params.context - Request context data.
                   * @returns {function} Params resolver
                   */
                  resolveParams({ context }) {
                    return params => ({
                      ...params,
                      review_id: context.review.id,
                    })
                  },
                },
              },
            },
            update: {
              method: 'PUT',
              path: '/api/feedback_reviews/:id',
              settings: {
                paramsResolver: {
                  /**
                   * We want to always include the reviewId, even when it's included upstream in
                   * the context.
                   *
                   * @param {object} params.context - Request context data.
                   * @returns {function} Params resolver
                   */
                  resolveParams({ context }) {
                    return params => ({
                      ...params,
                      review_id: context.review.id,
                    })
                  },
                },
              },
            },
            delete: {
              method: 'DELETE',
              path: '/api/feedback_reviews/:id',
              settings: {
                paramsResolver: {
                  /**
                   * We want to always include the reviewId, even when it's included upstream in
                   * the context.
                   *
                   * @param {object} params.context - Request context data.
                   * @returns {function} Params resolver
                   */
                  resolveParams({ context }) {
                    return params => ({
                      ...params,
                      review_id: context.review.id,
                    })
                  },
                },
              },
            },
          },
        },
      },
    },
    product: {
      endpoints: {
        review: {
          endpoints: {
            all: {
              method: 'GET',
              path: '/api/products/:product.id/reviews',
            },
            create: {
              method: 'POST',
              path: '/api/reviews',
              settings: {
                paramsResolver: {
                  /**
                   * We want to always include the productId, even when it's included upstream in
                   * the context.
                   *
                   * @param {object} params.context - Request context data.
                   * @returns {function} Params resolver
                   */
                  resolveParams({ context }) {
                    return params => ({
                      ...params,
                      product_id: context.product.id,
                    })
                  },
                },
              },
            },
            update: {
              method: 'PUT',
              path: '/api/reviews/:id',
              settings: {
                paramsResolver: {
                  /**
                   * We want to always include the productId, even when it's included upstream in
                   * the context.
                   *
                   * @param {object} params.context - Request context data.
                   * @returns {function} Params resolver
                   */
                  resolveParams({ context }) {
                    return params => ({
                      ...params,
                      product_id: context.product.id,
                    })
                  },
                },
              },
            },
          },
        },
      },
    },
    user: {
      endpoints: {
        review: {
          endpoints: {
            all: {
              method: 'GET',
              path: '/api/users/:user.id/reviews',
            },
          },
        },
      },
    },
  },
}

export default plugin
