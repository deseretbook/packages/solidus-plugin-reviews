# solidus-plugin-reviews

A [solidus-sdk](https://solidus-sdk.netlify.com) plugin that adds support for the [solidus_reviews](https://github.com/solidusio-contrib/solidus_reviews) gem.

## Before You Get Started

You should have the [solidus_reviews](https://github.com/solidusio-contrib/solidus_reviews) gem installed
on your instance of Solidus already. If you don't, then get it setup first before you continue.

You should also have [solidus-sdk](https://solidus-sdk.netlify.com) installed and ready to go as well.

## Installation

```
yarn install solidus-plugin-reviews
```

Then whenever you initialize a new instance of the solidus-sdk, you can include the plugin like so:

```
import { Solidus } from 'solidus-sdk'
import reviewsPlugin from 'solidus-plugin-reviews'

const instance = new Solidus({
  href: 'https://mysolidus.app',

  // Add the plugin here:
  plugins: [
    reviewsPlugin,
  ],
})
```

## API

All examples below assume the instance of solidus-sdk was initialized as above:

### Create a review

`[POST] /api/reviews`

Creates a new review.

```
instance.review().create(newReviewData)
```

### Get a review by id

`[GET] /api/reviews/:id`

Gets an existing review based on its `id`.

```
instance.review({ id: 1 }).get()
```

### Update a review

`[PUT] /api/reviews/:id`

Updates an existing review.

```
instance.review({ id: 1 }).update(changesToMake)
```

### Delete a review

`[DELETE] /api/reviews/:id`

Deletes an existing review.

```
instance.review({ id: 1 }).delete()
```

### Get all product reviews

`[GET] /api/products/:product.id/reviews`

Retrieves a paginated list of reviews for the given product.

Optionally pass in `per_page` and `page` to get paginated results.

```
instance.product({ id: 1 }).review().all({ per_page: 25, page: 1 })
```

### Create a product review

`[POST] /api/reviews`

Creates a new review for the given product. This automatically associates
the product id to the review.

```
instance.product({ id: 1 }).review().create(newReviewData)
```

### Update a product review

`[PUT] /api/reviews/:id`

Updates an existing review for the product. This automatically associates
the product id to the review.

```
instance.product({ id: 1 }).review({ id: 2 }).update(changesToMake)
```

### Get user reviews

`[GET] /api/users/:user.id/reviews`

Retrieves a list of reviews the user has created.

```
instance.user({ id: 1 }).review().all()
```

### Create feedback for a review

`[POST] /api/feedback_reviews`

Creates a new feedback_review for a given review

```
instance.review({ id: 2 }).feedback().create(newFeedback)
```

### Update feedback for a review

`[PUT] /api/feedback_reviews/:id`

Updates an existing feedback_review

```
instance.review({ id: 2 }).feedback({ id: 3 }).update(updatedFeedback)
```

### Destroy feedback for a review

`[DELETE] /api/feedback_reviews/:id`

Deletes an existing feedback_review

```
instance.review({ id: 2 }).feedback({ id: 3 }).delete({ userId: 4 })
```
